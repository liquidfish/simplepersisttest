using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;

namespace SimplePersistTest.Controllers {

    [Route("[controller]")]
    [ApiController]
    class PersonController : ControllerBase {
        
        [HttpGet]
        public ActionResult<string> getAll() {
            var strs = new List<string>();
            strs.Add("one");
            return Ok("test");
        }
    }
}